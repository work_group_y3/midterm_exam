import 'dart:io';

postfix(ep) {
  var val = ep.split('');
  var vals = new List<String>.filled(0, "", growable: true);

  for (int i = 0; i < val.length; i++) {
    if (val[i] != " ") {
      vals.add(val[i]);
    }
  }

  return vals;
}

String PostfixExpression(String exp) {
  var operators = new List<String>.empty(growable: true);
  var postfix = new List<String>.empty(growable: true);
  var val = exp.split('');
  var vals = new List<String>.filled(0, "", growable: true);
  String token = "";
  String ch;
  RegExp item = new RegExp("[a-zA-Z0-9.]+");

  for (int i = 0; i < val.length; i++) {
    if (val[i] != " ") {
      vals.add(val[i]);
    }
  }

  int precedence(String ch) {
    switch (ch) {
      case "+":
      case "-":
        return 1;
      case "*":
      case "x":
      case "X":
      case "/":
        return 2;
      case "^":
        return 3;
      default:
        return -1;
    }
  }

  for (var i = 0; i < vals.length; i++) {
    ch = vals[i];
    if (item.hasMatch(ch)) {
      token += ch;
    } else if (ch == '(') {
      operators.add(ch);
    } else if (ch == ')') {
      while (operators.isNotEmpty && operators.last != "(") {
        token += operators.removeLast();
      }
      if (operators.isNotEmpty) operators.removeLast();
    } else {
      while (operators.isNotEmpty &&
          precedence(ch) <= precedence(operators.last)) {
        token += operators.removeLast();
      }
      operators.add(ch);
    }
  }
  while (operators.isNotEmpty) {
    token += operators.removeLast();
    postfix.add(token);
  }

  return postfix.join(",");
}

void main() {
  print("Expression");
  String expression = (stdin.readLineSync()!);

  print("Infix:");
  print(postfix(expression));

  print("Postfix:");
  print(PostfixExpression(expression));
}
