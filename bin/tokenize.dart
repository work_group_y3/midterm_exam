import 'dart:io';
import 'dart:math';

postfix(ep) {
  var val = ep.split('');
  var vals = new List<String>.filled(0, "", growable: true);

  for (int i = 0; i < val.length; i++) {
    if (val[i] != " ") {
      vals.add(val[i]);
    }
  }

  return vals;
}

String PostfixExpression(String exp) {
  var operators = new List<String>.empty(growable: true);
  var postfix = new List<String>.empty(growable: true);
  var val = exp.split('');
  var vals = new List<String>.filled(0, "", growable: true);
  String token = "";
  String ch;
  RegExp item = new RegExp("[a-zA-Z0-9.]+");

  for (int i = 0; i < val.length; i++) {
    if (val[i] != " ") {
      vals.add(val[i]);
    }
  }

  for (var i = 0; i < vals.length; i++) {
    ch = vals[i];
    if (item.hasMatch(ch)) {
      token += ch;
    } else if (ch == '(') {
      operators.add(ch);
    } else if (ch == ')') {
      while (operators.isNotEmpty && operators.last != "(") {
        token += operators.removeLast();
      }
      if (operators.isNotEmpty) operators.removeLast();
    } else {
      while (operators.isNotEmpty &&
          precedence(ch) <= precedence(operators.last)) {
        token += operators.removeLast();
      }
      operators.add(ch);
    }
  }
  while (operators.isNotEmpty) {
    token += operators.removeLast();
    postfix.add(token);
  }

  return postfix.join(",");
}

num cal(String op, double b, double a) {
  switch (op) {
    case '+':
      return a + b;
    case '-':
      return a - b;
    case '*':
      return a * b;
    case '%':
      return a % b;
    case '^':
      return pow(a, b);
    case '/':
      return a / b;
  }
  return 0;
}

String Resul(String val) {
  RegExp reg = new RegExp("[0-9.]+");
  List<String> token = val.split("");
  List<String> resul = List.empty(growable: true);
  List<String> operators = List.empty(growable: true);

  for (var i = 0; i < token.length; i++) {
    if (token[i] == "") continue;

    if (reg.hasMatch(token[i])) {
      String x = "";
      while (i < token.length && reg.hasMatch(token[i])) x += token[i++];
      resul.add(x);
      i--;
    } else if (token[i] == "(") {
      operators.add(token[i]);
    } else if (token[i] == ")") {
      while (operators.last != "(") {
        resul.add(cal(operators.removeLast(), double.parse(resul.removeLast()),
                double.parse(operators.removeLast()))
            .toString());
      }
      operators.removeLast();
    } else if (token[i] == "+" ||
        token[i] == "-" ||
        token[i] == "*" ||
        token[i] == "%" ||
        token[i] == "^" ||
        token[i] == "/") {
      while (operators.isNotEmpty &&
          precedence(operators.last) >= precedence(token[i])) {
        resul.add(cal(operators.removeLast(), double.parse(resul.removeLast()),
                double.parse(resul.removeLast()))
            .toString());
      }
      operators.add(token[i]);
    }
  }

  while (operators.isNotEmpty) {
    resul.add(cal(operators.removeLast(), double.parse(resul.removeLast()),
            double.parse(resul.removeLast()))
        .toString());
  }

  return resul.removeLast();
}

int precedence(String ch) {
  switch (ch) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "x":
    case "X":
    case "/":
      return 2;
    case "^":
      return 3;
    default:
      return -1;
  }
}

void main() {
  print("Expression: ");
  String expression = (stdin.readLineSync()!);

  print("Infix: ");
  print(postfix(expression));

  print("Postfix: ");
  print(PostfixExpression(expression));

  print("Result: ");
  print(Resul(expression));
}
