import 'dart:io';

postfix(ep) {
  var val = ep.split('');
  var vals = new List<String>.filled(0, "", growable: true);

  for (int i = 0; i < val.length; i++) {
    if (val[i] != " ") {
      vals.add(val[i]);
    }
  }

  return vals;
}

void main() {
  print("Expression:");
  String expression = (stdin.readLineSync()!);

  print("Evalute Postfix:");
  print(postfix(expression));
}
